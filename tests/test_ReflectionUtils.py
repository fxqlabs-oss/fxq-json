from typing import List
from unittest import TestCase

from fxq._reflection_utils import _is_primitive
from fxq.json_requests import _get_generic, _is_list_with_generic


class TestExample:
    pass


class TestGenericOperations(TestCase):

    def test_given_list_with_type_when_get_generic_then_expect_returns_type(self):
        type = _get_generic(List[TestExample])
        self.assertEqual(TestExample, type)

    def test_given_list_with_type_when_is_list_with_generic_then_expect_true(self):
        self.assertTrue(_is_list_with_generic(List[TestExample]))

    def test_given_non_generic_when_is_list_with_generic_then_expect_false(self):
        self.assertFalse(_is_list_with_generic(TestExample))


class TestTypeOperations(TestCase):

    def test_given_string_when_is_primitive_then_expect_true(self):
        self.assertTrue(_is_primitive("Test"))

    def test_given_int_when_is_primitive_then_expect_true(self):
        self.assertTrue(_is_primitive(1))

    def test_given_float_when_is_primitive_then_expect_true(self):
        self.assertTrue(_is_primitive(1.1))

    def test_given_bool_when_is_primitive_then_expect_true(self):
        self.assertTrue(_is_primitive(True))

    def test_given_complex_type_when_is_primitive_then_expect_true(self):
        self.assertFalse(_is_primitive(TestExample))
