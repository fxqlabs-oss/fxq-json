from http import HTTPStatus
from unittest import TestCase
from unittest.mock import Mock, patch

from fxq._requests_adapter import _do_get_json


class TestRequestsAdapter(TestCase):

    def setUp(self) -> None:
        self.mock_200_response = Mock()
        self.mock_200_response.status_code = HTTPStatus.OK
        self.mock_200_response.json.return_value = {"key": "value"}

        self.mock_404_response = Mock()
        self.mock_404_response.status_code = HTTPStatus.NOT_FOUND

    def test_given_dummy_url_and_status_code_ok_when_get_expect_json_to_be_mock_json(self):
        with patch('requests.get', return_value=self.mock_200_response):
            self.assertEqual(_do_get_json("A_URL", params=None), {"key": "value"})

    def test_given_dummy_url_and_status_code_ok_when_get_expect_requests_get_to_be_called_once_with_url_and_params(
            self):
        with patch('requests.get', return_value=self.mock_200_response) as mock_get:
            _do_get_json("A_URL", params=None)
            mock_get.assert_called_once_with("A_URL", params=None)

    def test_given_dummy_url_and_status_code_ok_when_get_expect_response_json_to_be_called_once(self):
        with patch('requests.get', return_value=self.mock_200_response):
            _do_get_json("A_URL", params=None)
            self.mock_200_response.json.assert_called_once()

    def test_given_dummy_url_and_status_code_not_found_when_get_expect_exception(self):

        with patch('requests.get', return_value=self.mock_404_response) as mock_get:
            with self.assertRaises(Exception):
                _do_get_json("A_URL", params=None)
