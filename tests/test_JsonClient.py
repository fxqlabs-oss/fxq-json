import json
from pathlib import Path
from typing import List
from unittest import TestCase
from unittest.mock import patch, Mock

from fxq.json_requests import get

test_folder = Path(__file__).absolute().parent


class EmergencyContact:
    def __init__(self):
        self.name: str = None
        self.phone: str = None
        self.relationship: str = None


class Employee:
    types = {"emergencyContacts": List[EmergencyContact]}

    def __init__(self):
        self.id: int = None
        self.name: str = None
        self.hireDate: str = None
        self.current: bool = None
        self.emergencyContacts: List[EmergencyContact] = None


class TestJsonClientWithEmployee(TestCase):
    with open(f"{test_folder}/resources/employee.json", 'r') as ejf:
        employee_json = json.loads(ejf.read())

    @patch('fxq.json_requests._do_get_json', Mock(return_value=employee_json))
    def setUp(self) -> None:
        self.employee: Employee = get("GET_EMPLOYEE", resp_type=Employee)

    def test_employee_is_mapped(self):
        self.assertEqual(12345, self.employee.id)
        self.assertEqual("John Doe", self.employee.name)
        self.assertEqual("1980-01-02T00:00:00.000Z", self.employee.hireDate)
        self.assertEqual(True, self.employee.current)

    def test_employee_emergency_contacts_is_list(self):
        self.assertEqual(2, len(self.employee.emergencyContacts))

    def test_person_emergency_contact_1_is_mapped(self):
        self.assertEqual("Jane Doe", self.employee.emergencyContacts[0].name)
        self.assertEqual("888-555-1212", self.employee.emergencyContacts[0].phone)
        self.assertEqual("spouse", self.employee.emergencyContacts[0].relationship)

    def test_person_emergency_contact_2_is_mapped(self):
        self.assertEqual("Justin Doe", self.employee.emergencyContacts[1].name)
        self.assertEqual("877-123-1212", self.employee.emergencyContacts[1].phone)
        self.assertEqual("parent", self.employee.emergencyContacts[1].relationship)


class TestJsonClientWithEmployees(TestCase):
    with open(f"{test_folder}/resources/employees.json", 'r') as ejf:
        employees_json = json.loads(ejf.read())

    @patch('fxq.json_requests._do_get_json', Mock(return_value=employees_json))
    def setUp(self) -> None:
        self.employees: List[Employee] = get("GET_EMPLOYEES", resp_type=List[Employee])

    def test_employees_is_list(self):
        self.assertIsInstance(self.employees, list)

    def test_employees_contains_two_items(self):
        self.assertEqual(2, len(self.employees))

    def test_employee_1_is_mapped(self):
        self.assertEqual(1, self.employees[0].id)
        self.assertEqual("John Doe", self.employees[0].name)
        self.assertEqual("1980-01-02T00:00:00.000Z", self.employees[0].hireDate)
        self.assertEqual(True, self.employees[0].current)

    def test_employee_2_is_mapped(self):
        self.assertEqual(2, self.employees[1].id)
        self.assertEqual("Jane Doe", self.employees[1].name)
        self.assertEqual("1980-01-03T00:00:00.000Z", self.employees[1].hireDate)
        self.assertEqual(False, self.employees[1].current)
