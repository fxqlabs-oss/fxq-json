# FXQLabs-OSS json

fxq-json is a set of libraries and utils for working with json it includes json parsing utils and a requests 
adapter for for the requests library which handles json and performs marshalling of json responses back to objects of the given type

## Getting Started
First we need a class, well use an Employee exmaple

If our class is just simple types, [str, float, int, bool]. All we do is create the class as normal (See EmergencyContact example)

However if our class contains another class, we need to define this in a class level dictionary named types (See Employee example)

We use typings and generics to indicate a list

Let's take a look at an employee example:
```python
class EmergencyContact:
    def __init__(self):
        self.name: str = None
        self.phone: str = None
        self.relationship: str = None


class Employee:
    types = {"emergencyContacts": List[EmergencyContact]}

    def __init__(self):
        self.id: int = None
        self.name: str = None
        self.hireDate: str = None
        self.current: bool = None
        self.emergencyContacts: List[EmergencyContact] = None
```

Once we have defined our classes we simply call the get method like so:
```python
from fxq.json_requests import get

employee = get("http://some.api.site/employees/1", resp_type=Employee)
employees = get("http://some.api.site/employees", resp_type=List[Employee])

```

### Installing

Simply install the python module using 
```bash
pip install fxq-json
```

## Contributing

Contributions are most welcome to the project, please raise issues first and contribute in response to the issue with a pull request.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/fxqlabs-oss/fxq-json/downloads/?tab=tags). 

## Authors

* **Jonathan Turnock** - *Initial work* - [Bitbucket Profile](https://bitbucket.org/jonathan_turnock/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details