import pathlib

from setuptools import setup

setup(
    name='fxq-json',
    version='1.0.2-SNAPSHOT',
    packages=['fxq'],
    url='https://bitbucket.org/fxqlabs-oss/fxq-json',
    license='MIT',
    author='Jonathan Turnock',
    author_email='jonathan.turnock@outlook.com',
    description='Json marshalling utils including requests adapter when working with JSON APIs',
    long_description=(pathlib.Path(__file__).parent / "README.md").read_text(),
    long_description_content_type="text/markdown",
    install_requires=['requests', 'multidispatch', 'jsonpickle']
)
